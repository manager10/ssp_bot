# SSP_bot

A rasa bot for the website smartspeakerproducts.com

While you're waiting please add the following line to your terminal configuration  (depending on your operating system this is the '~/.bashrc' or '~/.zshrc' file).  This is needed so that you can access the embedded cluster using the 'kubectl' command  line interface.

	export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

Rasa X will be installed into the following Kubernetes namespace: rasa


Your Rasa X password is 9bSotIvHwMdTxMg7WWB5

The passwords for the other services in the deployment are:

Database password (PostgreSQL): fabar2nJSXYe6UEdbZc8
Event Broker password (RabbitMQ): UFYtAr7JS7ZHjIs42wQU
Lock Store password (Redis): 0uSOxygedvpB-GD9FDyh

You can now access Rasa X on this URL: http://34.116.79.60/login?username=me&password=9bSotIvHwMdTxMg7WWB5